package com.weixun.admin.controller;

import com.jfinal.core.Controller;
import com.jfinal.json.JFinalJson;
import com.jfinal.plugin.activerecord.Record;
import com.weixun.model.SysLog;
import com.weixun.admin.service.LogService;

import java.util.List;

public class LogController  extends Controller{
    LogService logService = new LogService();
    public void list(){
        List<SysLog> list= logService.find();
        renderJson(JFinalJson.getJson().toJson(list));
    }

    public void getlist(){
        List<Record> list= logService.findList();
        renderJson(JFinalJson.getJson().toJson(list));
    }

}
